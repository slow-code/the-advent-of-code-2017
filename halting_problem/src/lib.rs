use std::collections::HashMap;

pub enum State {
    A,
    B,
    C,
    D,
    E,
    F,
}

pub struct TuringMachine {
    state: State,
    tape: HashMap<i32, bool>,
    position: i32,
}


impl TuringMachine {
    
    pub fn initialise() -> Self {
        TuringMachine {
            state: State::A,
            position: 0,
            tape: HashMap::new(),
        }
    }

    pub fn update(&mut self) {

        let value = self.tape.entry(self.position).or_insert(false);
        
        let (wr, mv, goto) = match self.state {
            State::A => {
                if *value {
                    (false, -1, State::F)
                } else {
                    (true, 1, State::B)
                }
            }
            State::B => {
                if *value {
                    (false, 1, State::D)
                } else {
                    (false, 1, State::C)
                }
            }
            State::C => {
                if *value {
                    (true, 1, State::E)
                } else {
                    (true, -1, State::D)
                }
            }
            State::D => {
                if *value {
                    (false, -1, State::D)
                } else {
                    (false, -1, State::E)
                }
            }
            State::E => {
                if *value {
                    (true, 1, State::C)
                } else {
                    (false, 1, State::A)
                }
            }
            State::F => {
                if *value {
                    (true, 1, State::A)
                } else {
                    (true, -1, State::A)
                }
            }
        };

        *value = wr;
        self.position += mv;
        self.state = goto;

    }

    pub fn checksum(self) -> usize {
        self.tape.values().map(|x| *x as usize).sum()
    }
}
