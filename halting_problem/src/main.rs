use halting_problem::*;

fn main() {
    let steps = 12794428;
    let mut turing_machine = TuringMachine::initialise();

    for _ in 0..steps {
        turing_machine.update();
    }

    println!("Checksum: {}", turing_machine.checksum());
}
