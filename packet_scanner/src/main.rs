use packet_scanner::*;

fn main() {
    let scanners = parse_input("input.txt");

    let severity = scanners
        .iter()
        .filter(|scnr| scnr.detected(&0))
        .map(|scnr| scnr.severity())
        .sum::<u32>();

    println!("Part 1: Severity is {:?}", severity);

    let undetected = (0..)
        .find(|delay| !scanners.iter().any(|s| s.detected(&delay)))
        .unwrap();

    println!("Part 2: Minimum delay is {:?}", undetected);
}
