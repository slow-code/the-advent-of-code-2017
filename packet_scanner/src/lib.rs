use std::fs;

pub struct Scanner {
    layer: u32,
    range: u32,
}

impl Scanner {
    pub fn new(layer: &str, range: &str) -> Scanner {
        Scanner {
            layer: layer.parse().unwrap(),
            range: range.parse().unwrap(),
        }
    }

    pub fn detected(&self, delay: &u32) -> bool {
        let elapsed = self.layer + delay;
        let scan_range = (self.range - 1) * 2;
        elapsed % scan_range == 0
    }

    pub fn severity(&self) -> u32 {
        self.layer * self.range
    }
}

pub fn parse_input(filename: &str) -> Vec<Scanner> {
    let scanners = fs::read_to_string(filename).unwrap();
    scanners
        .lines()
        .map(|l| {
            let (layer, range) = l.split_once(": ").unwrap();
            Scanner::new(layer, range)
        })
        .collect::<Vec<Scanner>>()
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_1() {
        let s = Scanner { layer: 6, range: 4 };
        assert_eq!(s.detected(&0), true);
    }
    #[test]
    fn test_2() {
        let s = Scanner { layer: 4, range: 4 };
        assert_eq!(s.detected(&0), false);
    }
}
